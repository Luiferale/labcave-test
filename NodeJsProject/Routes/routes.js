/* Controllers */
const startingSceneController = require('../Controllers/startingSceneController');

/* Router Handler */
module.exports = (router) => {
  /* '/api/..' calls handler */
  router.use('/', (req, res, next) => {
    // eslint-disable-next-line no-console
    console.log('Incomming request to router');
    next();
  });

  /* /api/getTime call Handler */
  router.get('/getTime', startingSceneController.getTime);

  return router;
};
