/* Dependencies */
const dotenv = require('dotenv');

dotenv.config();
const express = require('express');
const routes = require('./Routes/routes');

/* Environment */
const port = process.env.PORT;

/* Initialization */
const app = express();
const router = express.Router();

/* Router call on '/api' */
app.use('/api', routes(router));

/* Error Handler */
app.use((err, req, res) => {
  if (err) {
    // eslint-disable-next-line no-console
    console.log(err);
    const response = {
      data: 'Ups!! An error was caught!',
      status: 500,
    };
    res.send(response);
  }
});

/* Start Application */
app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Listening on http://localhost:${port}`);
});
