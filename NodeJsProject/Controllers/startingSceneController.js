/* Dependencies */
const formater = require('date-and-time');

/**
 * getTime function to return the current time
 * @param {*} req
 * @param {*} res
 */
function getTime(req, res) {
  const date = new Date();
  const response = {
    data: formater.format(date, 'hh:mm:ss'),
    status: 200,
  };
  res.send(response);
}

/* Exporting Methods */
module.exports = {
  getTime,
};
