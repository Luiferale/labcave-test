# LabCave test

### Projec Structure

It is composed by three main folders, each one is a different project:
- NodeJsProject
The NodeJs service implemented with ExpressJs.

- NodeJsProjectNonExpress
The NodeJs service implemented with http built-in feature.

- UnityProject
The Unity project containing a single scene with calls to both NodeJs' services.

### Installation

1. Download the project.
2. To start the NodeJs service that implements ExpressJs:
    1. Open a terminal/command-line
    2. Navigate to `labcave-test/NodeJsProject/`
    3. Run `npm install`
    4. Run `npm start`
    5. It will be listening at port 9090. To change it modify .env file before step 4.
3. To start the NodeJs service that does not implement ExpressJs:
    1. Open a terminal/command-line
    2. Navigate to `labcave-test/NodeJsProjectNonExpress/`
    3. Run `npm install`
    4. Run `npm start`
    5. It will be listening at port 9092. To change it modify .env file before step 4.
4. To start the Unity Project:
    1. Open the directory `labcave-test/UnityProject/` as an Unity project.
    2. Open the scene `LabCaveTest` (Assets/Scenes/...).
    3. Navigate to the GameObject `NetworkManager` and change de ports as defined in the previous services. For default it is set to 9090 and 9092 respectively.
    4. Play the scene.

### Usage

The Scene has two butoms, one for the first service (ExpressJs) and another for the second service (Non ExpressJs).

The three projects can be running simultaneously

### Extras

- Both services are implemented (one with ExpressJs and the other one with http built-in feature).
- Both services implement the `date-and-time` package.
- Both services implement the `eslint` package.
- An error handler system is implemented in both the Unity project and the NodeJs Services.
- The Unity project is set to build for ARM64. The `.apk` is at `labcave-test/UnityProject/AndroidBuild/`
