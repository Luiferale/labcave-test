﻿/// <summary>
/// NetworkMessage Class designed to translate responses from services
/// </summary>
public class NetworkMessage
{
    public string data;
    public int status;
}
