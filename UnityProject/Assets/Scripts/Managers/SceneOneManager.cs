﻿using UnityEngine;
using TMPro;

/// <summary>
/// SceneOneManager Class to Manage the scene LabCaveTest behaviour
/// </summary>
public class SceneOneManager : MonoBehaviour
{
    // Singleton Pattern
    private static SceneOneManager instance = null;

    // Scene Objects' reference
    [SerializeField] private TMP_Text textTime;
    [SerializeField] private PopUpErrorHandler popUpError;

    // Singleton Pattern
    private void Awake()
    {
        if (SceneOneManager.GetInstance() != null && SceneOneManager.GetInstance() != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }

    // Singleton Pattern
    public static SceneOneManager GetInstance()
    {
        return instance;
    }

    /// <summary>
    /// GetTime function is to make a call to NetworkManager to send a request to the respective 
    /// service to retrive the current time
    /// </summary>
    /// <param name="isExpressJs"></param>
    public void GetTime(bool isExpressJs)
    {
        if (isExpressJs)
            NetworkManager.GetInstance().GetTimeForExpressJs();
        else
            NetworkManager.GetInstance().GetTimeForNonExpressJs();
    }

    /// <summary>
    /// SetTime function is to set the current time retrived from service into the scene
    /// </summary>
    /// <param name="time"></param>
    public void SetTime(string time)
    {
        textTime.SetText(time);
    }

    /// <summary>
    /// PopUpError function is to call the PopUpErrorHandler to activate/deactivate the PopUp
    /// </summary>
    /// <param name="error"></param>
    public void PopUpError(string error)
    {
        popUpError.HandleError(error);
    }
}
