﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// NetworkManager Class is designed to Manage communication with services
/// </summary>
public class NetworkManager : MonoBehaviour
{
    // Singleton Pattern
    private static NetworkManager instance = null;

    // Constant url
    private static string url = "http://localhost:";
    // Ports at which send requests
    [SerializeField] private string portServerNonEx = "9092";
    [SerializeField] private string portServerExpJs = "9090";

    //Constant Success code
    private static int CODE_SUCCESS = 200;
    //Constant Error Message
    private static string ERROR_MESSAGE = "Couldn't Connect To Server";

    // Singleton Pattern
    private void Awake()
    {
        if(NetworkManager.GetInstance() != null && NetworkManager.GetInstance() != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            NetworkManager.instance = this;
        }
    }

    // Singleton Pattern
    public static NetworkManager GetInstance()
    {
        return instance;
    }

    /// <summary>
    /// SetTime function is to sent the time returned from service to the scene
    /// </summary>
    /// <param name="result"></param>
    public void SetTime(string result)
    {
        SceneOneManager.GetInstance().SetTime(result);
    }

    /// <summary>
    /// GetTimeForNonExpressJs function is to prepare the request Get Time to send to service without ExpressJs implementation
    /// </summary>
    public void GetTimeForNonExpressJs()
    {
        string request = url + portServerNonEx + "/api/getTime";
        StartCoroutine(SendGetRequest(request, (result) => {
            SceneOneManager.GetInstance().SetTime(result);
        }));
    }

    /// <summary>
    /// GetTimeForExpressJs function is to prepare the request Get Time to send to service with ExpressJs implementation
    /// </summary>
    public void GetTimeForExpressJs()
    {
        string request = url + portServerExpJs + "/api/getTime";
        StartCoroutine(SendGetRequest(request, (result) => {
            SceneOneManager.GetInstance().SetTime(result);
        }));
    }

    /// <summary>
    /// SendGetRequest coroutine is a generic function to send 'GET' request to services
    /// </summary>
    /// <param name="url"></param>
    /// <param name="callback"></param>
    /// <returns></returns>
    IEnumerator SendGetRequest(string url, System.Action<string> callback)
    {
        UnityWebRequest request = UnityWebRequest.Get(url);
        yield return request.SendWebRequest();

        if (request.isNetworkError)
        {
            SceneOneManager.GetInstance().PopUpError(ERROR_MESSAGE);
        }
        else
        {
            NetworkMessage result = JsonUtility.FromJson<NetworkMessage>(request.downloadHandler.text);
            if(result.status == CODE_SUCCESS)
                callback(result.data);
            else
                SceneOneManager.GetInstance().PopUpError(result.data);
        }
    }
}
