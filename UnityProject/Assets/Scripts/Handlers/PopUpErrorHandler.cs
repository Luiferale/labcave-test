﻿using UnityEngine;
using TMPro;

/// <summary>
/// PopUpErrorHandler Class designed to control PopUpError GameObject's behaviour
/// </summary>
public class PopUpErrorHandler : MonoBehaviour
{
    [SerializeField] private TMP_Text textError;

    /// <summary>
    /// SetText function is to assign the error message into the PopUpError
    /// </summary>
    /// <param name="error"></param>
    private void SetText(string error)
    {
        string err = error ?? "";
        textError.SetText(err);
    }

    /// <summary>
    /// HandleError function is to control the PopUp's display
    /// </summary>
    /// <param name="error"></param>
    public void HandleError(string error)
    {
        gameObject.SetActive(!gameObject.activeSelf);
        SetText(error);
    }
}
