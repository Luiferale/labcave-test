/* Controllers */
const startingSceneController = require('../Controllers/startingSceneController');

/* Router Handler */
module.exports = (req, res) => {
  /* '/api/..' calls handler */
  // eslint-disable-next-line no-console
  console.log('Incomming request to router');
  switch (req.url) {
    case '/api/getTime':
      /* /api/getTime call Handler */
      startingSceneController.getTime(req, res);
      break;

    default:
      break;
  }
};
