/* Dependencies */
const dotenv = require('dotenv');

dotenv.config();
const http = require('http');
const routes = require('./Routes/routes');

/* Environment */
const port = process.env.PORT;

/* Initialization */
http.createServer((req, res) => {
  try {
    /* Routes call on '/api' */
    if (req.url.startsWith('/api')) {
      routes(req, res);
    }
  } catch (error) {
    /* Error Handler */
    // eslint-disable-next-line no-console
    console.log(error);
    const response = {
      data: 'Ups!! An error was caught!',
      status: 500,
    };
    res.write(JSON.stringify(response));
  } finally {
    res.end();
  }
}).listen(port, () => {
  /* Start Application */
  // eslint-disable-next-line no-console
  console.log(`Listening on http://localhost:${port}`);
});
